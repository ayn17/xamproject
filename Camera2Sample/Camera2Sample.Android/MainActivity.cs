﻿using Android;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Views;
using System;
using System.Collections.Generic;

namespace Camera2Sample.Droid
{
    [Activity(Label = "Camera2Sample", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {

        private static  int MAX_PREVIEW_WIDTH = 1920;
        private static  int MAX_PREVIEW_HEIGHT = 1080;

        public const int CameraPermissionsCode = 1;


        public static readonly string[] CameraPermissions =
        {
            Manifest.Permission.Camera
        };

        public static event EventHandler CameraPermissionGranted;
        private View _layout;


        // Storage Permissions
        public const int StoragePermissionsCode = 1;

        public static readonly string[] StoragePermissions =
        {
             Manifest.Permission.ReadExternalStorage,
             Manifest.Permission.WriteExternalStorage
        };
        public static event EventHandler StoragePermissionGranted;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);


            Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());

           // SetContentView(Resource.Layout.CameraLayout);

            _layout = FindViewById(Resource.Id.action_bar_root);
        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            if (requestCode == CameraPermissionsCode && grantResults[0] == Permission.Denied)
            {
                Snackbar.Make(_layout, "Camera permission is denied. Please allow Camera use.", Snackbar.LengthIndefinite)
                    .SetAction("OK", v => RequestPermissions(CameraPermissions, CameraPermissionsCode))
                    .Show();
                return;
            }
        }


        protected override void OnResume()
        {
            base.OnResume();

        }

        //CameraPermissionGranted?.Invoke(this, EventArgs.Empty);

        //if (requestCode == StoragePermissionsCode && grantResults[0] == Permission.Denied)
        //{
        //    Snackbar.Make(_layout, "Storage permission is denied. Please allow StorageS use.", Snackbar.LengthIndefinite)
        //        .SetAction("OK", v => RequestPermissions(StoragePermissions, StoragePermissionsCode))
        //        .Show();
        //    return;
        //}
        //StoragePermissionGranted?.Invoke(this, EventArgs.Empty);





        //if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == (int)Permission.Granted)
        //{
        //    StoragePermissionGranted?.Invoke(this, EventArgs.Empty);
        //}
        //else
        //{
        //    Snackbar.Make(_layout, "Storage permission is denied. Please allow StorageS use.", Snackbar.LengthIndefinite)
        //                       .SetAction("OK", v => RequestPermissions(StoragePermissions, StoragePermissionsCode))
        //                       .Show();
        //    return;
        //}


        //public void VerifyStoragePermissions(Activity activity)
        //{
        //    // Check if we have write permission
        //    int permission = (int)ActivityCompat.CheckSelfPermission(activity, Manifest.Permission.WriteExternalStorage);

        //    if (permission != PackageManager.PERMISSION_GRANTED)
        //    {
        //        // We don't have permission so prompt the user
        //        ActivityCompat.requestPermissions(
        //                activity,
        //                PERMISSIONS_STORAGE,
        //                REQUEST_EXTERNAL_STORAGE
        //        );
        //    }}

        // public bool ReadStoragePermissionGranted()  
        //{
        //    if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
        //    {
        //        if (CheckSelfPermission(Manifest.Permission.ReadExternalStorage)
        //                == PackageManager.CheckPermission(StoragePermissionGranted,))
        //        {
        //            //Log.v(TAG, "Permission is granted1");
        //            return true;
        //        }
        //        else
        //        {

        //          //  Log.v(TAG, "Permission is revoked1");
        //            ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.ReadExternalStorage }, 3);
        //            return false;
        //        }
        //    }
        //    else
        //    { //permission is automatically granted on sdk<23 upon installation
        //        //Log.v(TAG, "Permission is granted1");
        //        return true;
        //    }
        //}

        //  public bool WriteStoragePermissionGranted()
        //  {
        //if (Build.VERSION.SdkInt >= 23)
        //    if (Build.VERSION.SdkInt >= Build.VERSION_CODES.M)
        //    {
        //        if (CheckSelfPermission(Android.Manifest.Permission.WriteExternalStorage))
        //              //  == PackageManager.PERMISSION_GRANTED)
        //        {
        //            return true;
        //        }
        //        else
        //        {

        //            ActivityCompat.RequestPermissions(this, new String[] { Manifest.Permission.WriteExternalStorage }, 2);
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}


    }

}