﻿using Android.Hardware.Camera2;
using System;

namespace Camera2Sample.Droid.Renderers.Camera
{
	public class CameraCaptureStateListener : CameraCaptureSession.StateCallback
	{
		public Action<CameraCaptureSession> OnConfigureFailedAction;

		public Action<CameraCaptureSession> OnConfiguredAction;
        private CameraDroid cameraDroid;

        public CameraCaptureStateListener(CameraDroid cameraDroid)
        {
            this.cameraDroid = cameraDroid;
        }

        public override void OnConfigureFailed(CameraCaptureSession session)
        {
            if (null != cameraDroid.Context)
            { }
               // Toast.MakeText(fragment.Activity, "Failed", ToastLength.Short).Show();
        }

		public override void OnConfigured(CameraCaptureSession session)
		{
            cameraDroid.previewSession = session;
            cameraDroid.UpdatePreview();
        }
	}
}
