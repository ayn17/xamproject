﻿using Android.Content;
using Android.Util;
using Android.Views;
using Java.Lang;

namespace Camera2Sample.Droid.Renderers.Camera
{
	public class CameraTextureView : TextureView
	{
        //private int _mRatioWidth;
        //private int _mRatioHeight;
        private int ratioWidth = 0;
        private int ratioHeight = 0;

        public CameraTextureView(Context context) : base(context, null)
		{
		}

		public CameraTextureView(Context context, IAttributeSet attrs) :
			base(context, attrs, 0)
		{
		}

		public CameraTextureView(Context context, IAttributeSet attrs, int defStyle) :
			base(context, attrs, defStyle)
		{
		}

		public void SetAspectRatio(int width, int height)
		{

            if (width < 0 || height < 0)
                throw new Exception("Size cannot be negative.");
            ratioWidth = width;
            ratioHeight = height;
            RequestLayout();

            //if (width < 0 || height < 0)
            //	throw new IllegalArgumentException("Size cannot be negative.");

            //if (ratioWidth == width && ratioHeight == height)
            //	return;

            //ratioWidth = width;
            //ratioHeight = height;
            //RequestLayout();

        }

		protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
		{

            base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
            int width = MeasureSpec.GetSize(widthMeasureSpec);
            int height = MeasureSpec.GetSize(heightMeasureSpec);
            if (0 == ratioWidth || 0 == ratioHeight)
                SetMeasuredDimension(width, height);
            else
            {
                if (width < height * ratioWidth / ratioHeight)
                {
                    SetMeasuredDimension(width, width * ratioHeight / ratioWidth);
                }
                else
                {
                    SetMeasuredDimension(height * ratioWidth / ratioHeight, height);
                }
            }
            //base.OnMeasure(widthMeasureSpec, heightMeasureSpec);

            //var width = MeasureSpec.GetSize(widthMeasureSpec);
            //var height = MeasureSpec.GetSize(heightMeasureSpec);

            //if (ratioWidth == 0 || ratioHeight == 0)
            //{
            //	SetMeasuredDimension(width, height);
            //}
            //else
            //{
            //	if (width < height * ratioWidth / ratioHeight)
            //	{
            //		SetMeasuredDimension(width, width * ratioHeight / ratioWidth);
            //	}
            //	else
            //	{
            //		SetMeasuredDimension(height * ratioWidth / ratioHeight, height);
            //	}
            //}
        }
	}
}
