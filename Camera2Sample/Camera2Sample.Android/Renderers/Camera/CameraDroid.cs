﻿
using Android.Content;
using Android.Graphics;
using Android.Hardware.Camera2;
using Android.Hardware.Camera2.Params;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using Camera2Sample.Renderers;
using Java.Lang;
using Java.Util;
using Java.Util.Concurrent;
using System;
using System.Collections.Generic;
using System.IO;
using Xamarin.Forms;
using Size = Android.Util.Size;

namespace Camera2Sample.Droid.Renderers.Camera
{
    public class CameraDroid : FrameLayout, TextureView.ISurfaceTextureListener
    {
        private static readonly SparseIntArray Orientations = new SparseIntArray();

        public event EventHandler<ImageSource> Photo;

        public bool OpeningCamera { private get; set; }

        public CameraDevice CameraDevice;

        //private readonly CameraStateListener _mStateListener;
        private SurfaceTexture _surfaceTexture;
        public TextureView _cameraTexture;
        //private CameraTextureView _cameraTextureView;
        private Size _previewSize;
        private Size videoSize;
        private readonly Context _context;
        private CameraManager _manager;

        public CameraDevice cameraDevice;
        public CameraCaptureSession previewSession;
        public MediaRecorder mediaRecorder;

        private CameraStateListener stateListener;
        private MySurfaceTextureListener surfaceTextureListener;

        private bool isRecordingVideo;
        public Semaphore cameraOpenCloseLock = new Semaphore(1);

        public CaptureRequest previewRequest;
        private CaptureRequest.Builder previewRequestBuilder;

        private HandlerThread backgroundThread;
        private Handler backgroundHandler;

        private ImageReader imageReader;



        public CameraDroid(Context context) : base(context)
        {
            _context = context;
            var inflater = LayoutInflater.FromContext(context);

            if (inflater == null) return;
            var view = inflater.Inflate(Resource.Layout.CameraLayout, this);

            _cameraTexture = view.FindViewById<TextureView>(Resource.Id.cameraTexture);

            Orientations.Append((int)SurfaceOrientation.Rotation0, 0);
            Orientations.Append((int)SurfaceOrientation.Rotation90, 90);
            Orientations.Append((int)SurfaceOrientation.Rotation180, 180);
            Orientations.Append((int)SurfaceOrientation.Rotation270, 270);

            _cameraTexture.SurfaceTextureListener = this;

            stateListener = new CameraStateListener { Camera = this };
            //_cameraTexture.Click += (sender, args) => { TakePhoto(); };

        }


        public void StartPreview()
        {
            try
            {
                if (CameraDevice == null || !_cameraTexture.IsAvailable || _previewSize == null) 
                    return;

                var texture = _cameraTexture.SurfaceTexture;

                texture.SetDefaultBufferSize(_previewSize.Width, _previewSize.Height);
                var surface = new Surface(texture);

                previewRequestBuilder = CameraDevice.CreateCaptureRequest(CameraTemplate.Preview);
                previewRequestBuilder.AddTarget(surface);

                CameraDevice.CreateCaptureSession(new List<Surface> { surface },
                    new CameraCaptureStateListener(this)
                    {
                        OnConfigureFailedAction = session =>
                        {
                        },
                        OnConfiguredAction = session =>
                        {
                            previewSession = session;
                            UpdatePreview();
                        }
                    },
                    null);
            }
            //if (!_cameraTexture.IsAvailable)
            //    _cameraTexture.SurfaceTextureListener = surfaceTextureListener;

            ////if (cameraDevice == null || _cameraTexture.IsAvailable || null == _previewSize)
            ////    return;

            //try
            //{
            //    SetUpMediaRecorder();
            //    // SurfaceTexture texture = _cameraTexture.SurfaceTexture;
            //    //Assert.IsNotNull(texture);
            //    _surfaceTexture.SetDefaultBufferSize(_previewSize.Width, _previewSize.Height);
            //    previewRequestBuilder = cameraDevice.CreateCaptureRequest(CameraTemplate.Record);
            //    var surfaces = new List<Surface>();
            //    var previewSurface = new Surface(_surfaceTexture);
            //    surfaces.Add(previewSurface);
            //    previewRequestBuilder.AddTarget(previewSurface);

            //    var recorderSurface = mediaRecorder.Surface;
            //    surfaces.Add(recorderSurface);
            //    previewRequestBuilder.AddTarget(recorderSurface);

            //    cameraDevice.CreateCaptureSession(surfaces, new CameraCaptureStateListener(this), backgroundHandler);


            catch (CameraAccessException e)
            {
                //  e.PrintStackTrace();
            }
            catch (IOException e)
            {
                //   e.PrintStackTrace();
            }
        }
        
        public void OpenCamera()
        {
            if (_context == null || OpeningCamera)
            {
                return;
            }
            OpeningCamera = true;

            int width = _cameraTexture.Width;
            int height = _cameraTexture.Height;

            _manager = (CameraManager)_context.GetSystemService(Context.CameraService);
            try
            {
                //if (!cameraOpenCloseLock.TryAcquire(2500, TimeUnit.Milliseconds))
                //    throw new RuntimeException("Time out waiting to lock camera opening.");
                string cameraId = _manager.GetCameraIdList()[0];
                CameraCharacteristics characteristics = _manager.GetCameraCharacteristics(cameraId);
                StreamConfigurationMap map = (StreamConfigurationMap)characteristics.Get(CameraCharacteristics.ScalerStreamConfigurationMap);
                videoSize = ChooseVideoSize(map.GetOutputSizes(Class.FromType(typeof(MediaRecorder))));
                _previewSize = ChooseOptimalSize(map.GetOutputSizes(Class.FromType(typeof(MediaRecorder))), width, height, videoSize);
                int orientation = (int)Resources.Configuration.Orientation;
                //if (orientation == (int)Android.Content.Res.Orientation.Landscape)
                //{
                //    _cameraTexture.SetAspectRatio(_previewSize.Width, _previewSize.Height);
                //}
                //else
                //{
                //    _cameraTexture.SetAspectRatio(_previewSize.Height, _previewSize.Width);
                //}
                ConfigureTransform(width, height);
                mediaRecorder = new MediaRecorder();
                _manager.OpenCamera(cameraId, stateListener, null);

            }
            catch (CameraAccessException)
            {

            }

      

        }

     
        private Size ChooseVideoSize(Size[] choices)
        {
            foreach (Size size in choices)
            {
                if (size.Width == size.Height * 4 / 3 && size.Width <= 1000)
                    return size;
            }
            //  Log.Error(TAG, "Couldn't find any suitable video size");
            return choices[choices.Length - 1];
        }

        private Size ChooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio)
        {
            var bigEnough = new List<Size>();
            int w = aspectRatio.Width;
            int h = aspectRatio.Height;
            foreach (Size option in choices)
            {
                if (option.Height == option.Width * h / w &&
                    option.Width >= width && option.Height >= height)
                    bigEnough.Add(option);
            }

            if (bigEnough.Count > 0)
                return (Size)Collections.Min(bigEnough, new CompareSizesByArea());
            else
            {
                // Log.Error(TAG, "Couldn't find any suitable preview size");
                return choices[0];
            }
        }

        public void ConfigureTransform(int viewWidth, int viewHeight)
        {
            if (_surfaceTexture == null || _previewSize == null || _context == null) 
                return;

            var windowManager = _context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();

            var rotation = windowManager.DefaultDisplay.Rotation;
            var matrix = new Matrix();
            var viewRect = new RectF(0, 0, viewWidth, viewHeight);
            var bufferRect = new RectF(0, 0, _previewSize.Width, _previewSize.Height);

            var centerX = viewRect.CenterX();
            var centerY = viewRect.CenterY();

            if (rotation == SurfaceOrientation.Rotation90 || rotation == SurfaceOrientation.Rotation270)
            {
                bufferRect.Offset(centerX - bufferRect.CenterX(), centerY - bufferRect.CenterY());
                matrix.SetRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.Fill);

                matrix.PostRotate(90 * ((int)rotation - 2), centerX, centerY);
            }

            _cameraTexture.SetTransform(matrix);
            //if (null == Activity || null == _previewSize || null == textureView)
            //    return;

            //int rotation = 1;
            //var matrix = new Matrix();
            //var viewRect = new RectF(0, 0, viewWidth, viewHeight);
            //var bufferRect = new RectF(0, 0, _previewSize.Height, _previewSize.Width);
            //float centerX = viewRect.CenterX();
            //float centerY = viewRect.CenterY();
            //if ((int)SurfaceOrientation.Rotation90 == rotation || (int)SurfaceOrientation.Rotation270 == rotation)
            //{
            //    bufferRect.Offset((centerX - bufferRect.CenterX()), (centerY - bufferRect.CenterY()));
            //    matrix.SetRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.Fill);
            //    float scale = System.Math.Max(
            //        (float)viewHeight / _previewSize.Height,
            //        (float)viewHeight / _previewSize.Width);
            //    matrix.PostScale(scale, scale, centerX, centerY);
            //    matrix.PostRotate(90 * (rotation - 2), centerX, centerY);
            //}
            //_cameraTexture.SetTransform(matrix);
        }

        private void SetUpMediaRecorder()
        { 
            string path =  "video-" + DateTime.Now.ToString("yymmdd-hhmmss") + ".mp4";
            Java.IO.File file = new Java.IO.File(Context.GetExternalFilesDir(null), path);
            
            //if (null == Activity)
            //    return;
            mediaRecorder.SetAudioSource(AudioSource.Mic);
            mediaRecorder.SetVideoSource(VideoSource.Surface);
            mediaRecorder.SetOutputFormat(OutputFormat.Mpeg4);
            mediaRecorder.SetAudioEncoder(AudioEncoder.Aac);
            mediaRecorder.SetVideoEncoder(VideoEncoder.H264);
            mediaRecorder.SetVideoSize(videoSize.Width, videoSize.Height);
            mediaRecorder.SetVideoFrameRate(30);
            mediaRecorder.SetOutputFile(file);
            mediaRecorder.SetVideoEncodingBitRate(10000000);
            int rotation = 1; //(int)Activity.WindowManager.DefaultDisplay.Rotation;
            int orientation = Orientations.Get(rotation);
            mediaRecorder.SetOrientationHint(orientation);
            mediaRecorder.Prepare();
            //mediaRecorder.Start();
        }

        private void GetVideoFile(Context context)
        {
            string fileName = "video-" + DateTime.Now.ToString("yymmdd-hhmmss") + ".mp4"; //new filenamed based on date time
                                                                                          //  File file = new File(context.GetExternalFilesDir(null), fileName);
                                                                                          // return file;
        }

        private void StartRecordingVideo()
        {
            try
            {
                //UI
                //  buttonVideo.SetText(Resource.String.mr_controller_stop);
                isRecordingVideo = true;

                //Start recording
                mediaRecorder.Start();
            }
            catch (IllegalStateException e)
            {
                e.PrintStackTrace();
            }
        }

        public void stopRecordingVideo()
        {
            //UI
            isRecordingVideo = false;
            // buttonVideo.SetText(Resource.String.record);

            //if (null != Activity)
            //{
            //    Toast.MakeText(Activity, "Video saved: " + GetVideoFile(Activity),
            //        ToastLength.Short).Show();
            //}


            // Workaround for https://github.com/googlesamples/android-Camera2Video/issues/2
            CloseCamera();
            OpenCamera();
        }

        public void UpdatePreview()
        {
            if (null == cameraDevice)
                return;

            try
            {
                SetUpCaptureRequestBuilder(previewRequestBuilder);
                HandlerThread thread = new HandlerThread("CameraPreview");
                thread.Start();
                previewSession.SetRepeatingRequest(previewRequestBuilder.Build(), null, backgroundHandler);
            }
            catch (CameraAccessException e)
            {
                e.PrintStackTrace();
            }
        }

        private void SetUpCaptureRequestBuilder(CaptureRequest.Builder builder)
        {
            builder.Set(CaptureRequest.ControlMode, new Java.Lang.Integer((int)ControlMode.Auto));

        }

        private void CloseCamera()
        {
            try
            {
                cameraOpenCloseLock.Acquire();
                if (null != cameraDevice)
                {
                    cameraDevice.Close();
                    cameraDevice = null;
                }
                if (null != mediaRecorder)
                {
                    mediaRecorder.Release();
                    mediaRecorder = null;
                }
            }
            catch (InterruptedException e)
            {
                throw new RuntimeException("Interrupted while trying to lock camera closing.");
            }
            finally
            {
                cameraOpenCloseLock.Release();
            }
        }

        public void OnSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
        {
            _surfaceTexture = surface;

            ConfigureTransform(width, height);
            StartPreview();
        }

        public bool OnSurfaceTextureDestroyed(SurfaceTexture surface)
        {
            return true;
        }

        public void OnSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
        {
        }

        public void OnSurfaceTextureUpdated(SurfaceTexture surface)
        {
        }

        private class CompareSizesByArea : Java.Lang.Object, Java.Util.IComparator
        {
            public int Compare(Java.Lang.Object lhs, Java.Lang.Object rhs)
            {
                // We cast here to ensure the multiplications won't overflow
                if (lhs is Size && rhs is Size)
                {
                    var right = (Size)rhs;
                    var left = (Size)lhs;
                    return Long.Signum((long)left.Width * left.Height -
                        (long)right.Width * right.Height);
                }
                else
                    return 0;

            }
        }
        //private void TakePhoto()
        //{
        //	if (_context == null || CameraDevice == null) return;

        //	var characteristics = _manager.GetCameraCharacteristics(CameraDevice.Id);
        //	Size[] jpegSizes = null;
        //	if (characteristics != null)
        //	{
        //		jpegSizes = ((StreamConfigurationMap)characteristics.Get(CameraCharacteristics.ScalerStreamConfigurationMap)).GetOutputSizes((int)ImageFormatType.Jpeg);
        //	}
        //	var width = 480;
        //	var height = 640;

        //	if (jpegSizes != null && jpegSizes.Length > 0)
        //	{
        //		width = jpegSizes[0].Width;
        //		height = jpegSizes[0].Height;
        //	}

        //	var reader = ImageReader.NewInstance(width, height, ImageFormatType.Jpeg, 1);
        //	var outputSurfaces = new List<Surface>(2) { reader.Surface, new Surface(_viewSurface) };

        //	var captureBuilder = CameraDevice.CreateCaptureRequest(CameraTemplate.StillCapture);
        //	captureBuilder.AddTarget(reader.Surface);
        //	captureBuilder.Set(CaptureRequest.ControlMode, new Integer((int)ControlMode.Auto));

        //	var windowManager = _context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
        //	var rotation = windowManager.DefaultDisplay.Rotation;
        //	captureBuilder.Set(CaptureRequest.JpegOrientation, new Integer(Orientations.Get((int)rotation)));

        //	var readerListener = new ImageAvailableListener();

        //	readerListener.Photo += (sender, buffer) =>
        //	{
        //		Photo?.Invoke(this, ImageSource.FromStream(() => new MemoryStream(buffer)));
        //	};

        //	var thread = new HandlerThread("CameraPicture");
        //	thread.Start();
        //	var backgroundHandler = new Handler(thread.Looper);
        //	reader.SetOnImageAvailableListener(readerListener, backgroundHandler);

        //	var captureListener = new CameraCaptureListener();

        //	captureListener.PhotoComplete += (sender, e) =>
        //	{
        //		StartPreview();
        //	};

        //	CameraDevice.CreateCaptureSession(outputSurfaces, new CameraCaptureStateListener
        //	{
        //		OnConfiguredAction = session =>
        //		{
        //			try
        //			{
        //				_previewSession = session;
        //				session.Capture(captureBuilder.Build(), captureListener, backgroundHandler);
        //			}
        //			catch (CameraAccessException ex)
        //			{
        //				Log.WriteLine(LogPriority.Info, "Capture Session error: ", ex.ToString());
        //			}
        //		}
        //	}, backgroundHandler);
        //}


        //public void StartPreview()
        //{
        //	if (CameraDevice == null || !_cameraTexture.IsAvailable || _previewSize == null) return;

        //	var texture = _cameraTexture.SurfaceTexture;

        //	texture.SetDefaultBufferSize(_previewSize.Width, _previewSize.Height);
        //	var surface = new Surface(texture);

        //	_previewBuilder = CameraDevice.CreateCaptureRequest(CameraTemplate.Preview);
        //	_previewBuilder.AddTarget(surface);

        //	CameraDevice.CreateCaptureSession(new List<Surface> { surface },
        //		new CameraCaptureStateListener
        //		{
        //			OnConfigureFailedAction = session =>
        //			{
        //			},
        //			OnConfiguredAction = session =>
        //			{
        //				_previewSession = session;
        //				UpdatePreview();
        //			}
        //		},
        //		null);


        //}

        //private void ConfigureTransform(int viewWidth, int viewHeight)
        //{
        //	if (_viewSurface == null || _previewSize == null || _context == null) return;

        //	var windowManager = _context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();

        //	var rotation = windowManager.DefaultDisplay.Rotation;
        //	var matrix = new Matrix();
        //	var viewRect = new RectF(0, 0, viewWidth, viewHeight);
        //	var bufferRect = new RectF(0, 0, _previewSize.Width, _previewSize.Height);

        //	var centerX = viewRect.CenterX();
        //	var centerY = viewRect.CenterY();

        //	if (rotation == SurfaceOrientation.Rotation90 || rotation == SurfaceOrientation.Rotation270)
        //	{
        //		bufferRect.Offset(centerX - bufferRect.CenterX(), centerY - bufferRect.CenterY());
        //		matrix.SetRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.Fill);

        //		matrix.PostRotate(90 * ((int)rotation - 2), centerX, centerY);
        //	}

        //	_cameraTexture.SetTransform(matrix);
        //}

        //private void UpdatePreview()
        //{
        //	if (CameraDevice == null || _previewSession == null) return;

        //	_previewBuilder.Set(CaptureRequest.ControlMode, new Integer((int)ControlMode.Auto));
        //	var thread = new HandlerThread("CameraPreview");
        //	thread.Start();
        //	var backgroundHandler = new Handler(thread.Looper);

        //	_previewSession.SetRepeatingRequest(_previewBuilder.Build(), null, backgroundHandler);
        //}
    }
}
